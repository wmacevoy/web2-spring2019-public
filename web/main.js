"use strict";
console.log("Guessing Game");

import { mk } from './mk.js';
import { events } from './events.js';

class GuessingGame {

    mkForm() {
        let fragment = mk.fragment(mk.tag("form",{'id': this.myId + "_game"},
             mk.tag("input",{'id':this.myId+"_guess",'type':'text'}),
             mk.tag("input",
                    {'id': this.myId + "_submit", 'value': 
                    "guess", 'type':"submit"}),
            "hint: ", mk.tag("span", {'id': this.myId + "_result"}),
            "tries: ", mk.tag("span", {'id': this.myId + "_tries"})
        ));
        return fragment;
        }
    constructor(secret, myId) {
        this.myId = myId;

        let div = document.getElementById(this.myId);

        // if I don't control of myId, this can be anything...
        div.innerHTML = "";
        div.appendChild(this.mkForm());

        /*
        `
        <form id="bob"><script>...</script></form> <!-- ${this.myId}_game">
          <input id="${this.myId}_guess" name="guess" type="text" placeholder="guess a number">
          <input id="${this.myId}_submit" type="submit" value="guess">
          hint: <span id="${this.myId}_result">TBD</span>
          tries: <span id="${this.myId}_tries">0</span>
        </form>
        `;
 */
        this.secret = secret;

        this.tries = 0;
        

        let form = document.getElementById(this.myId + "_game");

        // wiring
        let object = this;
        form.addEventListener("submit", (event) => {
            let element = this;
            event.preventDefault();
            object.doSubmit(element);
        })

    }

    set result(value) {
        document.getElementById(this.myId + "_result").innerHTML = value;
    }

    get tries() {
        return parseInt(this.triesElement.innerHTML, 10);
    }
    get triesElement() {
        return document.getElementById(this.myId + "_tries");
    }
    set tries(value) {
        this.triesElement.innerHTML = value;
    }

    get guess() {
        return document.getElementById(this.myId + "_guess").value;
    }

    doSubmit(who) {

        console.log("doSubmit(" + JSON.stringify(who) + ")");
        if (this.guess == this.secret) {
            this.result = "correct!";
            won = true;
        } else {
            this.result = "nope";
        }
        this.tries = this.tries + 1;
        return false;

    }
}

events.onload(()=>{
    console.log("load guessing games");
    let guessers=document.getElementsByClassName("guess");

    for (let i=0; i<guessers.length; ++i) {
        let id="guessing-game-" + i;
        let secret=guessers[i].getAttribute("data-secret");
        guessers[i].id = id; 
        new GuessingGame(secret,id);
    }
});

