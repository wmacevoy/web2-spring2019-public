# Webpack init (old schoool)

First initialize (press enter for all questions)

```bash
npm init
```

Then install webpack (old because we are using an old version of node)
```bash
npm install --save webpack@1.15.0
```

Then create a html file as `index.html`
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Basic application with Webpack</title>
    <link href="main.css">
  </head>
  <body>
    <h1>WebPack works !!</h1>
    <script src="bundle.js"></script>
  </body>
</html>
```

Now create `main.js` with the content:
```javascript
console.log('Webpack working!');
```


To bundle `main.js` into `bundle.js` run:
```bash
node ./node_modules/webpack/bin/webpack.js main.js bundle.js
```

Open the `index.html` file useing "File->Open" in Chrome, then open the Dev tools in Chrome & you should see the console.log bit (Webpack working!).

# dev-server

To display static content with a local static web werver

```bash
npm install --save http-server
```

Edit your package.json file to add a "run" script:

```javascript
...
"scripts": {
    "dev-server": "http-server -a 127.0.0.1 # npm run dev-server",
    ...
  },
  ...
```

Run the server with "npm run dev-server"
