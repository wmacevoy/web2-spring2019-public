"use_strict"

let ON_LOAD_ACTIONS=[]

window.onload = () => {
    console.log("loading actions");
    for (let i=0; i<ON_LOAD_ACTIONS.length; ++i) {
        ON_LOAD_ACTIONS[i]();
    }
}
    

function onload(action) {
    console.log("future onload...");
    ON_LOAD_ACTIONS.push(action);
}

export const events = { 'onload' : onload };
