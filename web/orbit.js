import { mk } from './mk.js';
import { events } from './events.js';

class Orbit {
  mkCanvas() {
    let canvas = mk.tag("canvas", {
      'id': this.id + "-canvas",
      'width': 300,
      'height': 300
    });
    let fragment = mk.fragment(canvas);
    return fragment;
  }
  constructor(id, eggURL) {
    this.id = id;
    this.sun = new Image();
    this.moon = new Image();
    this.earth = new Image();
    this.egg = new Image();
    this.eggURL = eggURL;
    this.reveal = false;

    let div = document.getElementById(this.id);
    div.innerHTML = "";
    div.appendChild(this.mkCanvas());

    this.time = 0;
    this.ctx =  document.getElementById(this.id + '-canvas').getContext('2d');

    this.sun.src = 'https://mdn.mozillademos.org/files/1456/Canvas_sun.png';
    this.moon.src = 'https://mdn.mozillademos.org/files/1443/Canvas_moon.png';
    this.earth.src = 'https://mdn.mozillademos.org/files/1429/Canvas_earth.png';
    this.egg.src = this.eggURL;

    this.requestDraw();
  }

  requestDraw() {
    let object = this;
    window.requestAnimationFrame(() => {
      let element = this;
      object.draw(element);
    });
  }

  drawEarth() {
    this.ctx.beginPath();
    this.ctx.arc(150, 150, 105, 0, Math.PI * 2, false); // Earth orbit
    this.ctx.stroke();

    this.ctx.save();
    this.ctx.translate(150, 150);

    // Earth
    this.ctx.rotate(((2 * Math.PI) / 60) * this.time.getSeconds() + ((2 * Math.PI) / 60000) * this.time.getMilliseconds());
    this.ctx.translate(105, 0);
    this.ctx.fillRect(0, -12, 40, 24); // Shadow
    this.ctx.drawImage(this.earth, -12, -12);

    this.drawMoon();
    this.ctx.restore();
  }

  drawMoon() {
    this.ctx.save();
    this.ctx.rotate(((2 * Math.PI) / 6) * this.time.getSeconds() + ((2 * Math.PI) / 6000) * this.time.getMilliseconds());
    this.ctx.translate(0, 28.5);
    this.ctx.drawImage(this.moon, -3.5, -3.5);

    this.drawEgg();
    this.ctx.restore();
  }

  drawEgg() {
    if (this.reveal) {
      this.ctx.save();
      this.ctx.translate(0, 10);
      this.ctx.scale(0.05, 0.05);
      this.ctx.drawImage(this.egg, 0, 0);
      this.ctx.restore();
    }
  }

  draw(element) {
    this.time = new Date();
    this.ctx.globalCompositeOperation = 'destination-over';
    this.ctx.clearRect(0, 0, 300, 300); // clear canvas

    this.ctx.fillStyle = 'rgba(0, 0, 0, 0.4)';
    this.ctx.strokeStyle = 'rgba(0, 153, 255, 0.4)';
    this.drawEarth();

    this.ctx.drawImage(this.sun, 0, 0, 300, 300);

    this.requestDraw();
  }
}

events.onload(()=>{
  console.log("loading orbits");  
  let orbits=document.getElementsByClassName("orbit");

  for (let i=0; i<orbits.length; ++i) {
      let id="orbit-" + i;
      let egg=orbits[i].getAttribute("data-egg");
      orbits[i].id = id; 
      new Orbit(id,egg);
  }
});