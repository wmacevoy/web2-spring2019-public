"use strict";

function tag() {
    let name = (arguments.length >= 1) ? arguments[0] : null;
    let attributes = (arguments.length >= 2) ? arguments[1] : null;
    if (name == null) {
        throw new Error("must specify tag name");
    }
    let element = document.createElement(name);
    if (attributes != null) for (let key in attributes) {
        if (attributes.hasOwnProperty(key)) {
          element.setAttribute(key,attributes[key]);
        }
    }
    for (let i=2; i<arguments.length; ++i) {
        if (typeof(arguments[i]) == "object") {
            element.appendChild(arguments[i]);
        } else {
            element.appendChild(text(arguments[i]));
        }
    }
    return element;
}

// Ex: text("hello world")
function text(content) {
    return document.createTextNode(String(content));
}

// Ex: fragment("name: ", tag("input",{'type':'text'}))

function fragment() {
    let fragment = document.createDocumentFragment();
    for (let i=0; i<arguments.length; ++i) {
        if (typeof(arguments[i]) == "object") {
            fragment.appendChild(arguments[i]);
        } else {
            fragment.appendChild(text(arguments[i]));
        }
    }
    return fragment;
}

export const mk = { 'tag' : tag, 'text' : text, 'fragment' : fragment };
