"use strict";

// functions group code into reusable parts

function fibRecFunc(k) {
    switch(k) {
        case 0: 
            return 0; 
            break;
        case 1: 
            return 1; 
            break;
        case 2: 
            return 1; 
            break;
        default: 
            return fibfunc(k-1) + fibfunc(k-2); 
            break;
    }
}

function fibIterFunc(k) {
   let prev = 1; 
   let current = 1;
   let count = 1;

   while (count < k) {
      let next = prev + current;
      prev = current;
      current = next;
      count = count + 1;
   }

   return current;
}

class PantryItem {
    constructor(kind,location) {
        this.kind = kind;
        this.location = location;
        this.opened = false;
    }

    asHTML() {
        console.log("<tr>")
        console.log("<td>" + this.kind + "</td>");
        console.log("<td>" + this.location + "</td>");
        console.log("<td>" + this.opened + "</td>");
        console.log("</tr>")
    }
}

let beans = new PantryItem("beans","middle shelf");
let tuna = new PantryItem("tuna","middle shelf");
beans.asHTML();
tuna.asHTML();

