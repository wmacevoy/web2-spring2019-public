"use strict";

function desc(x) {
    return JSON.stringify(x) + "/" + typeof(x);
}
let x = true;

console.log("x=" + desc(x));

x = false;

console.log("x=" + desc(x));

var y = "hi"; // old school

console.log("y=" + desc(y));

y = "bye";

console.log("y=" + desc(y));

let z = 32;

console.log("z=" + desc(z));

z = Math.cos(Math.PI/2); // radians circle is 2PI, NOT 360 degrees.

z= "6.123233995736766e-17";

console.log("z=" + desc(z));

let a = null;

console.log("a=" + desc(a));

let b = null;

console.log("b=" + desc(b));

b = "null";

console.log("b=" + desc(b));

// comment

/* multi-line 
 comment */

 // typeof(c) creates an exception - must be undefined anywhere
console.log("typeof(d)=" + typeof(d));

let c = { kind: "pasta", location: "3rd shelf", opened: false };

console.log("c=" + desc(c));
console.log("c.kind=" + desc(c.kind));
console.log("c['opened']=" + desc(c['opened']));

class PantryItem {
    constructor(kind,location) {
        this.kind = kind;
        this.location = location;
        this.opened = false;
    }
}

let p0 = new PantryItem("olive oil", "middle shelf");
let p1 = new PantryItem("rice", "first shelf");

console.log("p0=" + desc(p0));


let fib = [1,1,2,3,5,8,13,21];

console.log("fib=" + desc(fib));
console.log("fib.length=" + desc(fib.length));
for (let i=0; i<fib.length; ++i) {
    console.log("fib[" + i + "]=" + desc(fib[i]));
}

let i=0;
while (i < fib.length) {
    if (i == 2) { ++i; continue; }
    console.log("fib[" + i + "]=" + desc(fib[i]));
    ++i;
}

function printFibArrayDoWhileBad() {
let i=0;
do {
    console.log("fib[" + i + "]=" + desc(fib[i]));
    ++i;    
} while (i <= fib.length);
}

function fibfunc(k) {
    if (k <= 1) {
        return 1;
    } else {
        return fibfunc(k-1) + fibfunc(k-2);
    }
}

console.log("fibfunc=" + desc(fibfunc));
console.log("fibfunc(0)=" + desc(fibfunc(0)))
console.log("fibfunc(1)=" + desc(fibfunc(1)));
console.log("fibfunc(2)=" + desc(fibfunc(2)));
console.log("fibfunc(3)=" + desc(fibfunc(3)));

